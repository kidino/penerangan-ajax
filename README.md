# Contoh Ringkas Menerangkan tentang Ajax

Contoh ringkas ini adalah dari KelasProgramming.com. KelasProgramming.com menawarkan video tutorial online untuk PHP, Codeigniter dan Laravel dalam bahasa Melayu.

Sila ke KelasProgramming.com untuk maklumat lanjut.

# Penerangan

Dengan Ajax, browser boleh berkomunikasi dengan server luar tanpa perlu membina semula halaman. Antaramuka dan UI yang telah dibina boleh kekal, dan browser boleh mendapatkan data secara **_tersembunyi di belakang_** dan memaparkan data apabila ianya diterima kemudian.

Ajax dapat melegakan keresahan pengguna apabila melihat halaman menjadi putih kerana perlu dibina semula. Dengan Ajax apabila hanya data yang dihantar, bukannya HTML penuh, ia juga menjimatkan keperluan data bandwidth, dan menjimatkan masa.

## Bahagian 1

Projek ini cuba menerangkan dan menunjukkan bagaimana menggunakan Ajax dengan contoh yang paling ringkas. 

`index.html` akan menunjukkan borang. Apabila butang **KIRA** ditekan, ianya akan memanggil fungsi `kira()`. 

Fungsi `kira()` akan :

1. mendapatkan data dari borang
2. memformatkannya dengan `FormData` 
3. dan menghantar data ke `bmi.php` secara ajax dengan fungsi `fetch`

Maklumat lanjut tentang `fetch` boleh didapati di sini :
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch 

## Bahagian 2

`bmi.php` adalah file PHP yang akan memproses data yang diterima. `bmi.php` boleh membaca data seperti data dari mana-mana borang / form seperti biasa.

Input yang diterima akan digunakan untuk mengira BMI. Hasil kiraan akan dihantar semula dalam format JSON.

## Bahagian 3

Di index.html, kita bersambung semula di baris `23` apabila `bmi.php` menghantar semula hasil kiraan BMI.

Kita memanggil fungsi `kemaskini_bmi()` apabila data diterima.

## Bahagian 4

Fungsi `kemaskini_bmi()` akan mengemaskini ruang BMI di HTML dengan nilai yang diterima daripada `bmi.php`.

